/*
 * flight_controller_main.cpp
 *
 *  Created on: 06 ����. 2016 �.
 *      Author: Sergey
 */

#include "hal_library.h"
#include "cmsis_os.h"
#include "drivers/usb_device/usb_device_context.h"
#include "usb/usbd_data_if.h"
#include "utils/debug.h"
#include "protocol/usb_protocol.h"
#include "NFramework/protocol/protocol_commands_processor.h"
#include "protocol/commands/cmd_macro.h"
#include <algorithm>
#include <cstdio>

usbd_data_interface data_interface;

uint32_t test_counter = 0;

struct protocol_usb_provider{
	static usbd_data_interface& get_data_interface(){
		return data_interface;
	}
};

using usb_request = stm32_framework::usb_protocol_request<protocol_usb_provider>;
using usb_response = stm32_framework::usb_protocol_response<protocol_usb_provider>;
protocol_commands_processor processor;

bool usb_control(uint8_t bRequest, uint8_t* data, uint16_t wLength){
	if(bRequest == 'r'){
		test_counter = 0;
	}
	return false;
}

void start_usb(){
	ASSERT(data_interface.construct(), "Failed to allocate USB data interface");

	static Usb_generic_device_context<1> ctx;
	ctx.vendor_id = 0x0300;
	ctx.product_id = 0x0c1b;
	ctx.language_id = 1033;
	ctx.ep0_size = endpoint0_size::s64;
	ctx.configuration_string = "Nettle ALT";
	ctx.interface_string = "Nettle ALT";
	ctx.product_string = "Nettle ALT";
	ctx.manufacturer_string = "Nettle";
	ctx.serial_number_string = "1F000000001F";

	ctx.set_interface(&data_interface, 0);

	data_interface.set_control_callback(usb_control);

	usb_device::start(&ctx);

	vTaskDelay(1000);
}

unknown_command_handler unknown_cmd_handler;

struct copter_commands {
	static constexpr uint32_t read_memory = FOURCC('M', 'e', 'm', 'R');
};

uint32_t tx_buffer[256];

class CMD_TYPE_NAME(MemR) : public protocol_command_handler {
public:
	bool prepare(protocol_request* request) override {
		request->receive_data(_mem_size);
		if((_mem_size == 0) || ((_mem_size % 4) != 0)){
			return false;
		}
		return true;
	}
	bool execute(protocol_response* response) override {
		uint32_t value = 0;
		while(_mem_size > 0){
			auto block_size = std::min<uint32_t>(_mem_size, sizeof(tx_buffer));
			//fill values
			for(uint32_t j = 0; j < block_size / 4; ++j){
				tx_buffer[j] = value++;
			}
			if(!response->send_data(&tx_buffer[0], block_size)){
				printf("Command interrupted\r\n");
				return false;
			}
			_mem_size-=block_size;
		}
		return true;
	}
private:
	uint32_t _mem_size;
};

CMD_VAR_DECL(MemR);

protocol_command_handler* get_command_handler(protocol_command_id_t id){
	switch(id){
	CMD_CASE(copter_commands::read_memory, MemR);
	default:
		printf("Unknown command!\r\n");
		return &CMD_VAR_NAME(unknown);
		break;
	}
}

extern "C" void flight_controller_main() {
	setvbuf(stdout, NULL, _IONBF, 0);
	printf("Begin initialization...\r\n");

	start_usb();

	while(true){
		if(data_interface.state() == data_interface_state::pending_reset){
			data_interface.abort_endpoints();
			printf("Protocol reset\r\n");
		}

		usb_request request;
		usb_response response;
		processor.process_command(&request, &response, &get_command_handler);
		//read packet
		/*while(!rx_ep.rx_receive_packet(true)){
			if(data_interface.state() == data_interface_state::pending_reset){
				data_interface.abort_endpoints();
			}
		}
		rx_ep.rx_read_buffer(&rx_buffer[0], sizeof(rx_buffer));

		//transmit packet
		for(int i = 0; i < TEST_PACKET_SIZE / 4; ++i){
			uint32_t* val = (uint32_t*)&tx_buffer[0];
			val[i] = test_counter++;
		}
		tx_ep.tx_buffer(&tx_buffer[0], sizeof(tx_buffer));
		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);*/
	}
}
