#ifndef HAL_LIBRARY_H__
#define HAL_LIBRARY_H__

#if defined(STM32F4)
#include "stm32f4xx_hal.h"
#elif defined(STM32F0)
#include "stm32f0xx_hal.h"
#elif defined(STM32F1)
#include "stm32f1xx_hal.h"
#else
#error "Unknown HAL library"
#endif

#endif //HAL_LIBRARY_H__
