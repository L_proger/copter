/*
 * nrf_stm32.h
 *
 *  Created on: 13 ���. 2016 �.
 *      Author: Sergey
 */

#ifndef NRF_STM32_H_
#define NRF_STM32_H_

#include "board.h"
#include "hal_library.h"
#include "nrf24l01p/nrf.h"


extern SPI_HandleTypeDef RADIO_SPI;

using namespace radio_cpp;

template<uint32_t CSN_port, uint16_t CSN_pin, uint32_t CE_port, uint16_t CE_pin, SPI_HandleTypeDef* SPI>
class Nrf_stm : public nrf24::Module {
public:
	void csn_low() override {
		HAL_GPIO_WritePin((GPIO_TypeDef*)CSN_port, CSN_pin, GPIO_PIN_RESET);
	}
	void csn_high() override {
		HAL_GPIO_WritePin((GPIO_TypeDef*)CSN_port, CSN_pin, GPIO_PIN_SET);
	}
	void ce_low() override{
		HAL_GPIO_WritePin((GPIO_TypeDef*)CE_port, CE_pin, GPIO_PIN_RESET);
	}
	void ce_high() override{
		HAL_GPIO_WritePin((GPIO_TypeDef*)CE_port, CE_pin, GPIO_PIN_SET);
	}
	void ce_pulse() {
		ce_high();
		HAL_Delay(1);
		ce_low();
	}
	void rw(const uint8_t* tx, uint8_t* rx, uint16_t length) override{
		HAL_SPI_TransmitReceive(SPI, const_cast<uint8_t*>(tx), rx, length, HAL_MAX_DELAY);
	}
};

typedef Nrf_stm<(uint32_t)RADIO_CSN_GPIO_Port, RADIO_CSN_Pin, (uint32_t)RADIO_CE_GPIO_Port, RADIO_CE_Pin, &RADIO_SPI> radio_t;


#endif /* NRF_STM32_H_ */
