#ifndef radio_protocol_h__
#define radio_protocol_h__

#include <cstddef>

#pragma pack(push, 1)
struct RadioPacketHeader {
	union {
		uint8_t value;
		struct {
			uint8_t pipe_id : 4;
			uint8_t is_idle : 1;
			uint8_t reserved : 3;
		};
	};
};

template<size_t MaxPayloadSize = 31>
struct RadioBuffer {
	uint8_t payload_length;
	RadioPacketHeader header;
	uint8_t payload[MaxPayloadSize];
};
#pragma pack(pop)

#endif // radio_protocol_h__
