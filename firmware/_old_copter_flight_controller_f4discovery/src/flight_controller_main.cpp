/*
 * flight_controller_main.cpp
 *
 *  Created on: 19 ���. 2016 �.
 *      Author: Sergey
 */

#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include <cstdio>
#include "nrf_stm32.h"
#include "radio_protocol.h"

radio_t radio;
extern "C" void MX_USART2_UART_Init(void);

extern "C" void flight_controller_main(){
	MX_USART2_UART_Init();
	printf("Flight controller F4Discovery\r\n");

	printf("%f\r\n",1.234567f);


	radio.init();
	uint8_t addr_p0[5];
	radio.read_multibyte_reg((uint8_t)pipe_id::pipe0, &addr_p0[0]);
	printf("rx_addr_p0: 0x%02x%02x%02x%02x%02x\r\n", (int)addr_p0[4], (int)addr_p0[3], (int)addr_p0[2], (int)addr_p0[1], (int)addr_p0[0]);

	vTaskDelay(1000);

	radio.set_irq_mode(irq_source_t::max_rt, true);
	radio.set_irq_mode(irq_source_t::rx_dr, true);
	radio.set_irq_mode(irq_source_t::tx_ds, true);
	radio.open_pipe(pipe_id::pipe0, true);
	radio.set_auto_retr(15, 250);
	radio.enable_dynamic_payload(true);
	radio.set_datarate(nrf24::datarate_t::mbps_2);
	radio.set_address_width(nrf24::address_width_t::aw_3bytes);
	radio.enable_dynamic_ack(true);
	radio.enable_ack_payload(true);
	radio.set_mode(Mode::tx);

	while(true){
		HAL_GPIO_TogglePin(LD6_GPIO_Port, LD6_Pin);
		vTaskDelay(1000);

	}
}
