/*
 * board.h
 *
 *  Created on: 13 ���. 2016 �.
 *      Author: Sergey
 */

#ifndef BOARD_H_
#define BOARD_H_

#include "stm32f4xx_hal.h"
#include "cmsis_os.h"

#define UART_STDIO
#define CONSOLE_UART huart3
extern UART_HandleTypeDef CONSOLE_UART;

#define RADIO_SPI hspi2

#endif /* BOARD_H_ */
