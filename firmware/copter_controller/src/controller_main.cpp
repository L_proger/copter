/*
 * controller_main.cpp
 *
 *  Created on: 13 ���. 2016 �.
 *      Author: Sergey
 */
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "nrf_stm32.h"
#include "radio_protocol.h"

radio_t radio;

uint32_t tx_size = 0;
uint8_t tx_buffer[32];
uint8_t rx_buffer[32];
uint64_t tx_size_total = 0;


uint64_t rx_size_total = 0;

void send_packet(){
	while(!radio.tx_fifo_full()){
		radio.write_tx_payload(tx_buffer, sizeof(tx_buffer));
	}
}

QueueHandle_t tx_queue;
QueueHandle_t rx_queue;

void send_packet(uint8_t pipe_id, const uint8_t* data, uint8_t data_len){
	if(data_len > 31){
		printf("Too big TX payload\r\n");
		return;
	}

	RadioBuffer<> buffer;
	buffer.header.pipe_id = pipe_id;
	buffer.header.is_idle = 0;
	buffer.header.reserved = 0;
	buffer.payload_length = data_len;
	for(uint8_t i = 0; i < data_len; ++i){
		buffer.payload[i] = data[i];
	}
	xQueueSend(tx_queue, &buffer, portMAX_DELAY);
}


//

bool receive_packet(RadioBuffer<>* buffer, TickType_t timeout = portMAX_DELAY){
	if(xQueueReceive(rx_queue, (void*)buffer, timeout)){
		portENTER_CRITICAL();
		while(!radio.rx_fifo_empty()){
			if(uxQueueSpacesAvailable(rx_queue) == 0){
				break;
			}
			RadioBuffer<> rx_buffer;
			rx_buffer.payload_length = radio.read_rx_payload((uint8_t*)&rx_buffer.header);
			if(!rx_buffer.header.is_idle){
				xQueueSend(rx_queue, &rx_buffer, portMAX_DELAY);
			}
		}
		portEXIT_CRITICAL();
		return true;
	}
	return false;
}

uint32_t idle_counter = 0;

void send_idle(){
	RadioPacketHeader idle_packet;
	idle_packet.value = 0;
	idle_packet.is_idle = 1;
	radio.write_tx_payload((uint8_t*)&idle_packet, sizeof(idle_packet));
	++idle_counter;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	if(GPIO_Pin == RADIO_IRQ_Pin){
		auto status = radio.clear_irq_flags_get_status();

		if(status.rx_dr == 1){
			while(!xQueueIsQueueFullFromISR(rx_queue)){
				if(radio.rx_fifo_empty()){
					break;
				}
				RadioBuffer<> rx_buffer;
				rx_buffer.payload_length = radio.read_rx_payload((uint8_t*)&rx_buffer.header);
				if(!rx_buffer.header.is_idle){
					xQueueSendFromISR(rx_queue, &rx_buffer, nullptr);
				}
			}
		}

		if(status.tx_ds == 1){
			RadioBuffer<> buffer;
			while(!radio.tx_fifo_full()){
				if(xQueueReceiveFromISR(tx_queue, &buffer, nullptr)){
					radio.write_tx_payload((uint8_t*)&buffer.header, buffer.payload_length + sizeof(buffer.header));
					tx_size_total += buffer.payload_length;
				}else{
					send_idle();
				}
			}
		}

		if(status.max_rt == 1){
			radio.reuse_tx();
			printf("max_rt\r\n");
		}
	}
}

TaskHandle_t xHandle = nullptr;

uint8_t test_data[31];
void RadioTxTask(void* pvParameters){
	for(;;){
		send_packet(1, test_data, sizeof(test_data));
		send_packet(3, test_data, sizeof(test_data));
	}
}


extern "C" void controller_main(){
	setvbuf(stdout, NULL, _IONBF, 0);
	printf("Hello!\r\n");

	tx_queue = xQueueCreate(3, sizeof(RadioBuffer<>));
	rx_queue = xQueueCreate(3, sizeof(RadioBuffer<>));

	radio.init();
	uint8_t addr_p0[5];
	radio.read_multibyte_reg((uint8_t)pipe_id::pipe0, &addr_p0[0]);
	printf("rx_addr_p0: 0x%02x%02x%02x%02x%02x\r\n", (int)addr_p0[4], (int)addr_p0[3], (int)addr_p0[2], (int)addr_p0[1], (int)addr_p0[0]);

	radio.set_irq_mode(irq_source_t::max_rt, true);
	radio.set_irq_mode(irq_source_t::rx_dr, true);
	radio.set_irq_mode(irq_source_t::tx_ds, true);
	radio.open_pipe(pipe_id::pipe0, true);
	radio.set_auto_retr(15, 250);
	radio.enable_dynamic_payload(true);
	radio.set_datarate(nrf24::datarate_t::mbps_2);
	radio.set_address_width(nrf24::address_width_t::aw_3bytes);
	radio.enable_dynamic_ack(true);
	radio.enable_ack_payload(true);
	radio.set_mode(Mode::tx);


	xTaskCreate( RadioTxTask, "RadioTxTask", 600, nullptr, 2, &xHandle );
	configASSERT( xHandle );

	send_idle();
	uint32_t rx_old = 0;
	uint32_t tx_old = 0;

	while(true){
		HAL_GPIO_TogglePin(LED_3_GPIO_Port, LED_3_Pin);

		printf("RX speed: %i, TX speed: %i\r\n", (int)(rx_size_total - rx_old), (int)(tx_size_total - tx_old));
		rx_old = rx_size_total;
		tx_old = tx_size_total;

		/*portENTER_CRITICAL();
		auto status = radio.reg_read<nrf24::reg::status>();
		portEXIT_CRITICAL();*/
		//printf("TX total: %i RX total: %i\r\n", (int)tx_size_total, (int)rx_size_total);
		//printf("rx_p_no: %i, tx_full: %i, max_rt: %i tx_ds: %i rx_dr: %i\r\n", (int)status.rx_p_no, (int)status.tx_full, (int)status.max_rt, (int)status.tx_ds, (int)status.rx_dr );

		vTaskDelay(1000);
	}
}



