/*
 * motor.cpp
 *
 *  Created on: 08 ����. 2015 �.
 *      Author: Sergey
 */

#include "motor.h"
#include <algorithm>

Motor::Motor()
	:_channel(0), _pwm_timer(nullptr){
}

void Motor::init(TIM_HandleTypeDef* timer, uint32_t channel){
	_pwm_timer = timer;
	_channel = channel;
}


void Motor::set_speed(uint8_t speed_percent){
	speed_percent = std::min<uint8_t>(100, speed_percent);
	auto ccr_value = (_pwm_timer->Instance->ARR * speed_percent) / 100;

	if(speed_percent == 0){
		HAL_TIM_PWM_Stop(_pwm_timer, _channel);
	}else{
		switch (_channel){
		case TIM_CHANNEL_1:
			_pwm_timer->Instance->CCR1 = ccr_value;
			break;
		case TIM_CHANNEL_2:
			_pwm_timer->Instance->CCR2 = ccr_value;
			break;
		case TIM_CHANNEL_3:
			_pwm_timer->Instance->CCR3 = ccr_value;
			break;
		case TIM_CHANNEL_4:
			_pwm_timer->Instance->CCR4 = ccr_value;
			break;
		}

		HAL_TIM_PWM_Start(_pwm_timer, _channel);
	}
}
