/*
 * copter_main.cpp
 *
 *  Created on: 02 ���. 2016 �.
 *      Author: Sergey
 */

#include "stm32f1xx_hal.h"
#include "uart_protocol.h"
#include "uart_commands.h"
#include "motor.h"
#include "sound_module.h"
#include "nrf_stm32.h"
#include "radio_protocol.h"
//#include "bma_accelerometer.h"

extern TIM_HandleTypeDef htim1;
extern UART_HandleTypeDef huart3;
extern SPI_HandleTypeDef hspi2;
/*
bma_accelerometer<&hspi2> accelerometer;

UartCommand rx_cmd0  __attribute__ ((aligned (4)));
UartCommand rx_cmd1  __attribute__ ((aligned (4)));
UartCommand* rx_cmds[] = {&rx_cmd0, &rx_cmd1};


SoundNode armed_sound[6] = {
	{SoundNodeType::Wave, Notes::C() << 4, SOUND_MS_TO_SAMPLES(150)},
	{SoundNodeType::Silence, 1, SOUND_MS_TO_SAMPLES(55)},
	{SoundNodeType::Wave, Notes::F_d() << 4, SOUND_MS_TO_SAMPLES(150)},
	{SoundNodeType::Silence, 1, SOUND_MS_TO_SAMPLES(55)},
	{SoundNodeType::Wave, Notes::B() << 4, SOUND_MS_TO_SAMPLES(150)},
	{SoundNodeType::Silence, 1, SOUND_MS_TO_SAMPLES(355)}
};


dac_sound_player player;*/

/*
uint32_t active_cmd = 0;
volatile UartCommand* received_command = nullptr;

void begin_cmd_rx(){
	if(HAL_OK != HAL_UART_Receive_DMA(&huart3,(uint8_t*)rx_cmds[active_cmd], sizeof(UartCommand))){
		for(;;);
	}
}
extern "C" void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	received_command = rx_cmds[active_cmd];
	active_cmd = active_cmd == 0 ? 1 : 0;
	begin_cmd_rx();
}


UartCommand* try_get_command(){
	auto copy = received_command;
	if(copy != nullptr){
		received_command = nullptr;
	}
	return (UartCommand*)copy;
}

Motor motors[4];

void execute_command(UartCommand* cmd){
	UartCommandReader reader(cmd);

	switch(cmd->header.id){
	case uart_commands::led_read:{
			auto led_id = reader.read<uint8_t>();
			bool state = false;
			if(led_id == 0) {
				//state = HAL_GPIO_ReadPin(LED_BLUE_GPIO_Port, LED_BLUE_Pin) == GPIO_PIN_SET;
			}else{
				state = HAL_GPIO_ReadPin(LED_GREEN_GPIO_Port, LED_GREEN_Pin) == GPIO_PIN_SET;
			}

			UartCommandWriter writer(cmd);
			writer.write(led_id).write(state);
			HAL_UART_Transmit(&huart3, (uint8_t*)cmd, sizeof(*cmd), HAL_MAX_DELAY);
		}
		break;
	case uart_commands::led_write:{
			auto led_id = reader.read<uint8_t>();
			auto led_state = reader.read<bool>();
			if(led_id == 0) {
				//HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, led_state ? GPIO_PIN_SET : GPIO_PIN_RESET);
			}else{
				HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, led_state ? GPIO_PIN_SET : GPIO_PIN_RESET);
			}
			HAL_UART_Transmit(&huart3, (uint8_t*)cmd, sizeof(*cmd), HAL_MAX_DELAY);
		}
		break;
	case uart_commands::motor_write:{
			auto motor_id = reader.read<uint8_t>();
			auto motor_speed = reader.read<uint8_t>();
			motors[motor_id].set_speed(motor_speed);
			HAL_UART_Transmit(&huart3, (uint8_t*)cmd, sizeof(*cmd), HAL_MAX_DELAY);
		}
		break;
	case uart_commands::play_sound:{
			player.play_sound(armed_sound);
			HAL_UART_Transmit(&huart3, (uint8_t*)cmd, sizeof(*cmd), HAL_MAX_DELAY);
		}
		break;
	case uart_commands::acc_read:{
			auto acc = accelerometer.get_acceleration();
			UartCommandWriter writer(cmd);
			writer.write(acc);
			HAL_UART_Transmit(&huart3, (uint8_t*)cmd, sizeof(*cmd), HAL_MAX_DELAY);
		}
		break;
	default:
		while(true){
			HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
			HAL_Delay(500);
		}

		break;
	}
}*/

radio_t radio;

uint32_t rx_size = 0;
uint8_t rx_buffer[32];
uint8_t tx_buffer[32];

uint64_t rx_size_total = 0;

uint8_t ack_pipe_id = 3;

uint32_t rx_pipe_counters[] = {0,0,0,0,0,0,0};
uint32_t rx_idle_counter = 0;

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	if(GPIO_Pin == RADIO_IRQ_Pin){
		auto status = radio.clear_irq_flags_get_status();

		if(status.rx_dr == 0){
			printf("No data in RX radio! Something is really wrong!\r\n");
			while(true);
		}else{
			if(status.rx_p_no != 7){
				do{
					rx_size = radio.read_rx_payload(rx_buffer);
					rx_size_total += rx_size;

					RadioPacketHeader hdr;
					hdr.value = rx_buffer[0];

					if(hdr.is_idle){
						rx_idle_counter++;
					}else{
						rx_pipe_counters[hdr.pipe_id]++;
					}


				}while(!radio.rx_fifo_empty());

				while(!radio.tx_fifo_full()){
					RadioPacketHeader hdr;
					hdr.is_idle = 0;
					hdr.pipe_id = ack_pipe_id;
					hdr.reserved = 0;

					tx_buffer[0] = hdr.value;
					ack_pipe_id = ack_pipe_id == 3 ? 1 : 3;
					radio.write_ack_payload(0, tx_buffer, 15);
				}
			}else{
				radio.flush_rx();
			}
		}
	}
}

extern "C" void copter_main(){
	setvbuf(stdout, NULL, _IONBF, 0);
	printf("Hello!\r\n");

	radio.init();

	radio.set_irq_mode(irq_source_t::max_rt, false);
	radio.set_irq_mode(irq_source_t::rx_dr, true);
	radio.set_irq_mode(irq_source_t::tx_ds, false);
	radio.open_pipe(pipe_id::pipe0, true);
	radio.set_auto_retr(15, 250);
	radio.enable_dynamic_payload(true);
	radio.enable_dynamic_ack(true);
	radio.enable_ack_payload(true);
	radio.set_address_width(nrf24::address_width_t::aw_3bytes);
	radio.set_datarate(nrf24::datarate_t::mbps_2);
	radio.set_mode(Mode::rx);


	uint8_t addr_p0[5];
	radio.read_multibyte_reg((uint8_t)pipe_id::pipe0, &addr_p0[0]);
	printf("rx_addr_p0: 0x%02x%02x%02x%02x%02x\r\n", (int)addr_p0[4], (int)addr_p0[3], (int)addr_p0[2], (int)addr_p0[1], (int)addr_p0[0]);

	uint32_t old_rx = 0;
	while(true){
		HAL_Delay(1000);
		HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);

		printf("RX speed[byte/s]: %i,  p0:%i  p1:%i  p2:%i  p3:%i  p4:%i  idle:%i\r\n", (int)(rx_size_total - old_rx),
				(int)rx_pipe_counters[0],
				(int)rx_pipe_counters[1],
				(int)rx_pipe_counters[2],
				(int)rx_pipe_counters[3],
				(int)rx_pipe_counters[4], (int)rx_idle_counter);

		old_rx = rx_size_total;
	}


	/*motors[0].init(&htim1, TIM_CHANNEL_1);
	motors[0].set_speed(0);

	motors[1].init(&htim1, TIM_CHANNEL_2);
	motors[1].set_speed(0);

	motors[2].init(&htim1, TIM_CHANNEL_3);
	motors[2].set_speed(0);

	motors[3].init(&htim1, TIM_CHANNEL_4);
	motors[3].set_speed(0);


	HAL_GPIO_WritePin(SS_GYRO_GPIO_Port, SS_GYRO_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(SS_ACC_GPIO_Port, SS_ACC_Pin, GPIO_PIN_SET);

	accelerometer.init();

	if(!accelerometer.set_power(true)){
		for(;;);
	}

	player.play_sound(armed_sound);

	begin_cmd_rx();
	while(true){
		UartCommand* cmd = nullptr;

		//wait command
		while((cmd = try_get_command()) == nullptr){}

		execute_command(cmd);
	}*/
}
