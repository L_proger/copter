/*
 * syscalls.c
 *
 *  Created on: 15 θών 2015 γ.
 *      Author: Sergey
 */

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stm32f1xx_hal.h>
#include "stm32f1xx_it.h"
#include "board.h"

extern UART_HandleTypeDef CONSOLE_UART;

extern int errno;
register char * stack_ptr asm("sp");

caddr_t _sbrk(int incr) {
	extern char end asm("end");
	static char *heap_end;
	char *prev_heap_end;

	if (heap_end == 0)
		heap_end = &end;

	prev_heap_end = heap_end;
	if (heap_end + incr > stack_ptr) {
		errno = ENOMEM;
		return (caddr_t) -1;
	}

	heap_end += incr;
	return (caddr_t) prev_heap_end;
}


#ifndef __errno_r
#include <sys/reent.h>
#define __errno_r(reent) reent->_errno
#endif


int _write_r(struct _reent *r, int file, char * ptr, int len){
  (void)r;
  (void)file;
  (void)ptr;

  while (HAL_UART_GetState(&CONSOLE_UART) == HAL_UART_STATE_BUSY_TX ||
		  HAL_UART_GetState(&CONSOLE_UART) == HAL_UART_STATE_BUSY_TX_RX)
  {

  }

  //HAL_UART_Transmit_IT(&CONSOLE_UART, (uint8_t*)ptr, len);
  HAL_UART_Transmit(&CONSOLE_UART, (uint8_t*)ptr, len, HAL_MAX_DELAY);

  return len;
}

int _read_r(struct _reent *r, int file, char * ptr, int len)
{
  (void)r;
  (void)file;
  (void)ptr;
  (void)len;
  __errno_r(r) = EINVAL;
  return -1;
}

int _lseek_r(struct _reent *r, int file, int ptr, int dir){
  (void)r;
  (void)file;
  (void)ptr;
  (void)dir;
  return 0;
}

int _close_r(struct _reent *r, int file){
    (void)r;
    (void)file;
    return 0;
}

int _open_r ( struct _reent *ptr, const char *file, int flags, int mode ){
   int fd = -1;
   return fd;
}

int _fstat_r(struct _reent *r, int file, struct stat * st){
    (void)r;
    (void)file;
    memset(st, 0, sizeof(*st));
    st->st_mode = S_IFCHR;
    return 0;
}

int _isatty_r(struct _reent *r, int fd){
    (void)r;
    (void)fd;
    return 1;
}

int _kill (int a, int b){
	(void)a;
	(void)b;
	return 0;
}

int _getpid(int a){
	(void)a;
	return 0;
}

void _exit(int status){
	for(;;);
}
