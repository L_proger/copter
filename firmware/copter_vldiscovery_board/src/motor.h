/*
 * motor.h
 *
 *  Created on: 08 ����. 2015 �.
 *      Author: Sergey
 */

#ifndef SRC_MOTOR_H_
#define SRC_MOTOR_H_

#include <cstdint>
#include "stm32f1xx_hal.h"

class Motor {
public:
	Motor();
	void init(TIM_HandleTypeDef* timer, uint32_t channel);
	void set_speed(uint8_t speed_percent);
private:
	uint32_t _channel;
	TIM_HandleTypeDef* _pwm_timer;
};


#endif /* SRC_MOTOR_H_ */
