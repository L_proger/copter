/*
 * sound_module.h
 *
 *  Created on: 03 ���. 2016 �.
 *      Author: Sergey
 */

#ifndef SOUND_MODULE_H_
#define SOUND_MODULE_H_

#include "stm32f1xx_hal.h"
#include "sound.h"

/*
#define SOUND_SAMPLING_RATE 24000
#define SOUND_MS_TO_SAMPLES(__ms) ((SOUND_SAMPLING_RATE * __ms) / 1000)

extern DAC_HandleTypeDef hdac;
extern TIM_HandleTypeDef htim6;

class DacSoundPlayerBase {
public:
	static constexpr uint32_t get_sampling_rate() {
		return SOUND_SAMPLING_RATE;
	}
	static void on_sound_play() {
		HAL_DAC_Start(&hdac, DAC_CHANNEL_1);
		HAL_TIM_Base_Start_IT(&htim6);
	}
	static void on_sound_stop() {
		HAL_TIM_Base_Stop_IT(&htim6);
		HAL_DAC_Stop(&hdac, DAC_CHANNEL_1);
	}
};


typedef SoundPlayer<DacSoundPlayerBase> dac_sound_player;

*/

#endif /* SOUND_MODULE_H_ */
