/*
 * uart_commands.h
 *
 *  Created on: 02 ���. 2016 �.
 *      Author: Sergey
 */

#ifndef UART_COMMANDS_H_
#define UART_COMMANDS_H_

#include <cstdint>
#include "uart_protocol.h"

struct uart_commands {
	static constexpr uint32_t led_write = FOURCC('L', 'e', 'd', 'W');
	static constexpr uint32_t led_read = FOURCC('L', 'e', 'd', 'R');
	static constexpr uint32_t motor_write = FOURCC('M', 't', 'r', 'W');
	static constexpr uint32_t play_sound = FOURCC('P', 'S', 'n', 'd');
	static constexpr uint32_t acc_read = FOURCC('A', 'c', 'c', 'R');
	static constexpr uint32_t gyro_read = FOURCC('G', 'r', 'o', 'R');
};


#endif /* UART_COMMANDS_H_ */
