/*
 * accelerometer.h
 *
 *  Created on: 08 ����. 2015 �.
 *      Author: Sergey
 */

#ifndef SRC_ACCELEROMETER_H_
#define SRC_ACCELEROMETER_H_

#include "NMath.h"

using namespace NettleMath;

class Accelerometer  {
public:
	virtual float3 get_acceleration() = 0;
};


#endif /* SRC_ACCELEROMETER_H_ */
