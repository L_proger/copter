/*
 * uart_protocol.h
 *
 *  Created on: 02 ���. 2016 �.
 *      Author: Sergey
 */

#ifndef UART_PROTOCOL_H_
#define UART_PROTOCOL_H_

#include <cstdint>

#pragma pack(push, 1)
struct UartCommandHeader {
	uint32_t id;
	uint8_t payloadSize;
};

struct UartCommand {
	UartCommandHeader header;
	static constexpr int MaxPayloadSize = 32;
	uint8_t payload[MaxPayloadSize];
};

#define FOURCC(a,b,c,d) ( (uint32_t) ((((uint32_t)d)<<24) | (((uint32_t)c)<<16) | (((uint32_t)b)<<8) | ((uint32_t)a)) )

#pragma pack(pop)

struct UartCommandReader{
	uint32_t offset;
	UartCommand* cmd;
	UartCommandReader(UartCommand* _cmd):offset(0), cmd(_cmd){
	}

	template<typename T>
	T read(){
		return *((T*)&cmd->payload[offset++]);
	}
};

struct UartCommandWriter{
	UartCommand* cmd;
	UartCommandWriter(UartCommand* _cmd) : cmd(_cmd){
		cmd->header.payloadSize = 0;
	}

	template<typename T>
	UartCommandWriter& write(const T& val){
		((T*)&cmd->payload[cmd->header.payloadSize])[0] = val;
		cmd->header.payloadSize += sizeof(T);
		return *this;
	}
};

#endif /* UART_PROTOCOL_H_ */
