﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Ports;
using System.Runtime.InteropServices;

namespace uart_protocol_client {



    public class Program {

        public static void SetMotorSpeed(SerialPort copterPort, byte motor, byte speed)
        {
            copterPort.ExecuteCommand(UartCommand.Create(uart_commands.motor_write).Write<byte>(motor).Write<byte>(speed));
        }
        
        public static void Main(string[] args){
            var portName = "COM5";
            var portSpeed = 1500000;
            var copterPort = new SerialPort(portName, portSpeed, Parity.None);

            copterPort.ReadTimeout = SerialPort.InfiniteTimeout;
            copterPort.WriteTimeout = SerialPort.InfiniteTimeout;
           // copterPort.ReceivedBytesThreshold = UartCommand.MaxPayloadSize;
            copterPort.Open();




            while (true)
            {
                Console.Write("Enter motor id and speed percent: ");

                var param = Console.ReadLine();

                var p = param.Split(new [] {' '}, StringSplitOptions.RemoveEmptyEntries);
                if (p.Length == 2)
                {
                    SetMotorSpeed(copterPort, byte.Parse(p[0]), byte.Parse(p[1]));
                }
                else
                {
                    break;
                }
            }

            copterPort.Close();

        
        }
    }
}
