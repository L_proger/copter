
#include "sound.h"
#include <vector>
#include <iostream>

#define SOUND_SAMPLING_RATE 256000
#define SOUND_MS_TO_SAMPLES(__ms) ((SOUND_SAMPLING_RATE * __ms) / 1000)

class SoftwarePlayerBase
{
public:
	static constexpr uint32_t get_sampling_rate() {
		return SOUND_SAMPLING_RATE;
	}
	static void on_sound_play() {
		
	}
	static void on_sound_stop() {
		
	}
};

SoundPlayer<SoftwarePlayerBase> player;


static constexpr size_t octave = 1;
SoundNode track[1] = {
	{ SoundNodeType::Wave, 500 * 100, 256},
};

int main()
{
	std::vector<uint8_t> samples;

/*	for (int i = 0; i < 256; ++i)
	{
		auto sine_s = (int32_t)get_sine(i) + 0x7fff;
		uint32_t sine_u = ((uint32_t)sine_s) >> 8;
		samples.push_back(sine_u);
	}*/
	



	player.play_sound(track);
	while(player.is_playing)
	{
		player.update();
		samples.push_back(player.out);
	}

	for (int i = 0; i < samples.size(); ++i)
	{
		auto sine = (int32_t)get_sine(i / 2);
		auto sample = (int32_t)samples[i];

		auto diff = sine - sample;
		std::cout << diff << std::endl;
	}
    return 0;
}

